(function(){
	'use strict';

	angular.module('bnw', ['ui.router','bnw-main','bnw-services','templates', 'ngAnimate'])
	  .config(function ($stateProvider, $urlRouterProvider) {
	    $urlRouterProvider
	      .otherwise("/");
	  });
	  
})();