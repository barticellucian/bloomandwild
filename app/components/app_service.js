(function(){
	'use strict';

	angular.module('bnw-services', [])
		.factory('httpservice', ['$http', function($http){
			//CORS needs to be enabled so this could work
			return{
				getItems: function(callback, errback){
	  				$http.jsonp("https://api.bloomandwild.com/v1/collections")
	  				.success(callback)
	  				.error(errback);
	  			}
			};
		}]);
})();