(function(){
	'use strict';

	angular.module('bnw', ['ui.router','bnw-main','bnw-services','templates', 'ngAnimate'])
	  .config(function ($stateProvider, $urlRouterProvider) {
	    $urlRouterProvider
	      .otherwise("/");
	  });
	  
})();
'app controller goes here';
(function(){
	'use strict';

	angular.module('bnw-services', [])
		.factory('httpservice', ['$http', function($http){
			//CORS needs to be enabled so this could work
			return{
				getItems: function(callback, errback){
	  				$http.jsonp("https://api.bloomandwild.com/v1/collections")
	  				.success(callback)
	  				.error(errback);
	  			}
			};
		}]);
})();
(function(){
  'use strict';


  angular.module('bnw-main',['ui.router', 'ui.slider'])
    .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
      $stateProvider
        .state('main', {
          url: '/',
          templateUrl: 'main/main.html',
          controller: 'MainCtrl'
        });
        $locationProvider.html5Mode(true);
    })
    .controller('MainCtrl', function ($scope, httpservice, $timeout) {
        $scope.CartItems = [
          {
            "id":1,
            "name": "Item1",
            "image": "http://lorempixel.com/400/400/nature/",
            "price": 10
          },
          {
            "id":2,
            "name": "Item2",
            "image": "http://lorempixel.com/400/400/nature/",
            "price": 15
          },
          {
            "id":3,
            "name": "Item3",
            "image": "http://lorempixel.com/400/400/nature/",
            "price": 12
          },
          {
            "id":4,
            "name": "Item4",
            "image": "http://lorempixel.com/400/400/nature/",
            "price": 20
          },
          {
            "id":5,
            "name": "Item5",
            "image": "http://lorempixel.com/400/400/nature/",
            "price": 8
          },
          {
            "id":6,
            "name": "Item6",
            "image": "http://lorempixel.com/400/400/nature/",
            "price": 17
          },
          {
            "id":7,
            "name": "Item7",
            "image": "http://lorempixel.com/400/400/nature/",
            "price": 14
          }
        ];

        $scope.activeItemIndex = 0;
        $scope.deliveries = 1;
        $scope.totalPrice = $scope.deliveries * $scope.CartItems[$scope.activeItemIndex].price;
        $scope.deliveriesObj = {};

        $scope.updateDeliveries = function(index){
          $timeout(function(){
            $scope.deliveriesObj[index] = $scope.deliveries;
            $scope.totalPrice = $scope.deliveries * $scope.CartItems[$scope.activeItemIndex].price;
          });
        };

        $scope.changeItem = function(index){
          $timeout(function(){
            $scope.activeItemIndex = index;
            $scope.deliveries = $scope.deliveriesObj[index] || 1; 
            $scope.totalPrice = $scope.deliveries * $scope.CartItems[$scope.activeItemIndex].price;
          });
        };

        $scope.$watch('deliveries', function(newValue, oldValue) {
            $scope.updateDeliveries($scope.activeItemIndex);
        });
    });

})();